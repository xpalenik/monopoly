package sk.stuba.fei.uim.oop;

public class KartaSancaDedictvo implements KartaSanca {
    @Override
    public void vykonajUlohu(Hrac hrac) {
        System.out.println("Zomrel bohatý strýko. Zdedil si 500 eur.");
        hrac.vlozPeniaze(500);
    }
}
