package sk.stuba.fei.uim.oop;

public class HracImpl implements Hrac {

    private String meno;
    private int mnozstvoPenazi = 300;

    public String getMeno() {
        return meno;
    }

    @Override
    public int vypisZostatok() {
        return mnozstvoPenazi;
    }

    public HracImpl(String meno) {
        this.meno = meno;
    }

    @Override
    public void vlozPeniaze(int suma) {
        mnozstvoPenazi+=suma;
    }

    @Override
    public void vyberPeniaze(int suma) {
        if ((mnozstvoPenazi-suma) < 0) {
            throw new HracNemaPeniazeException();
        }

        mnozstvoPenazi-=suma;
    }
}
