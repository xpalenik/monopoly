package sk.stuba.fei.uim.oop;

import java.io.IOException;
import java.util.*;

import static sk.stuba.fei.uim.oop.KeyboardInput.*;

public class HraImpl implements Hra {

    private static final int SUMA_OBDRZANA_PRI_PRECHODE_STARTOM = 200;
    private static final int VYSKA_DANE = 50;

    private List<Policko> hernaPlocha;
    private Queue<Hrac> hraci;
    private Map<String,Integer> poziciaHraca;

    private static final int POLICOK_NA_PLOCHE = 24;

    private static final int INDEX_START = 0;
    private Policko getStart(){
        return hernaPlocha.get(INDEX_START);
    }
    private static final int INDEX_VAZENIE = 3*6;
    //private Vazenie getVazenie() {return (Vazenie) hernaPlocha.get(INDEX_VAZENIE);}

    Vazenie vazenie = new VazenieImpl();

    /**
     * Posunie hraca o dany pocet policok.
     *
     * @param hrac
     * @param posuvanychPolicok Pocet policok o ktore posunieme hraca.
     * @return True ak hrac presiel startom, false ak nepresiel.
     */
    public boolean posunHraca(Hrac hrac, int posuvanychPolicok){
        int pozicia = poziciaHraca.get(hrac.getMeno());
        poziciaHraca.put(hrac.getMeno(),(pozicia+posuvanychPolicok) % POLICOK_NA_PLOCHE);
        return pozicia + posuvanychPolicok >= POLICOK_NA_PLOCHE;
    }

    public void presunDoVazenia(Hrac hrac) {
        poziciaHraca.put(hrac.getMeno(),INDEX_VAZENIE);
    }

    private void nacitajHracov() {

        int pocetHracov = 0;
        while (pocetHracov<=0) {
            pocetHracov = readInt("Vlozte pocet hracov (napr. 4) a stlacte ENTER");
            if (pocetHracov<=0) System.out.println("Pocet hracov musi byt kladne cele cislo.");
        }

        hraci = new LinkedList<>();
        for (int i = 1; i <= pocetHracov; i++) {
            String meno = readString("Vlozte meno hraca cislo " + i);
            hraci.add(new HracImpl(meno));
        }

    }

    private void vypisHracov() {
        System.out.println("Vlozili ste nasledovnych hracov: ");

        for (Hrac hrac : hraci) {
            System.out.println(hrac.getMeno());
        }
    }

    private void vytvorHernuPlochu() {

        Queue<KartaSanca> balicekKarietSanca = new LinkedList<>();
        balicekKarietSanca.add(new KartaSancaOdmena());
        balicekKarietSanca.add(new KartaSancaDanZaSkolu());
        balicekKarietSanca.add(new KartaSancaDedictvo());
        balicekKarietSanca.add(new KartaSancaVyhra());
        balicekKarietSanca.add(new KartaSancaPoplatky());

        poziciaHraca = new HashMap<>();

        hernaPlocha = new ArrayList<>();

        hernaPlocha.add(new StartImpl());
        hernaPlocha.add(new SancaImpl(balicekKarietSanca));
        hernaPlocha.add(new NehnutelnostImpl(10));
        hernaPlocha.add(new NehnutelnostImpl(80));
        hernaPlocha.add(new NehnutelnostImpl(150));
        hernaPlocha.add(new NehnutelnostImpl(30));

        hernaPlocha.add(new PlatbaDaneImpl(VYSKA_DANE));
        hernaPlocha.add(new NehnutelnostImpl(70));
        hernaPlocha.add(new SancaImpl(balicekKarietSanca));
        hernaPlocha.add(new NehnutelnostImpl(110));
        hernaPlocha.add(new NehnutelnostImpl(30));
        hernaPlocha.add(new NehnutelnostImpl(60));

        hernaPlocha.add(new PoliciaImpl(vazenie, this));
        hernaPlocha.add(new NehnutelnostImpl(30));
        hernaPlocha.add(new NehnutelnostImpl(170));
        hernaPlocha.add(new SancaImpl(balicekKarietSanca));
        hernaPlocha.add(new NehnutelnostImpl(40));
        hernaPlocha.add(new NehnutelnostImpl(140));

        hernaPlocha.add(new VazenieImpl());
        hernaPlocha.add(new NehnutelnostImpl(30));
        hernaPlocha.add(new NehnutelnostImpl(200));
        hernaPlocha.add(new NehnutelnostImpl(50));
        hernaPlocha.add(new SancaImpl(balicekKarietSanca));
        hernaPlocha.add(new NehnutelnostImpl(40));
    }

    private void umiestniHracovNaStart() {
        for (Hrac hrac : hraci){
            poziciaHraca.put(hrac.getMeno(),INDEX_START);
        }
    }

    private void vypisZostatkyHracov() {
        for (Hrac hrac: hraci){
            System.out.println("Zostatok hraca "+hrac.getMeno()+ " je " + hrac.vypisZostatok());
        }
    }

    private void vypisPozicieHracov() {
        for (Hrac hrac: hraci){
            System.out.println("Hrac "+hrac.getMeno()+ " je na " + (poziciaHraca.get(hrac.getMeno())+1) + ". policku.");
        }
    }

    private void pressEnterKeyToContinue()
    {
        System.out.println("Stlacte ENTER pre pokracovanie...");
        try
        {
            System.in.read();
        }
        catch (IOException e){
            System.out.println("Error reading from user");
        }
    }

    /**
     * Konštruktor vytvorí všetky prvky hracej plochy (napr. karty Šanca, všetky políčka).
     */
    public HraImpl() {
        nacitajHracov();
        vypisHracov();
        vytvorHernuPlochu();
        umiestniHracovNaStart();

        while (true) {
            System.out.println("\nZacina nove kolo!");

            if (hraci.size()<=1) {
                System.out.println("Koniec hry.");

                if (hraci.size()==1){
                    System.out.println("Vyhrava hrac " + hraci.poll().getMeno());
                }

                System.exit(0);
            }

            vypisPozicieHracov();
            vypisZostatkyHracov();
            vazenie.zostavaKol();

            pressEnterKeyToContinue();

            for (int i = 1; i <= hraci.size(); i++) {
                Hrac hracNaTahu = hraci.poll();
                if (vazenie.jeVoVazeni(hracNaTahu)){
                    hraci.add(hracNaTahu);
                    continue;
                }
                dalsieKolo(hracNaTahu);
                pressEnterKeyToContinue();
            }

            vazenie.presloJednoKolo();
        }
    }

    private int hodKockou() {
        Random rand = new Random();
        return 1+rand.nextInt(6);
    }

    private String typPolicka(Object o ){
        return o.getClass().getInterfaces()[0].getSimpleName();
    }

    public void dalsieKolo(Hrac hrac) {

        int vysledokHoduKockou = hodKockou();
        System.out.println("Hrac " + hrac.getMeno() + " hodil na kocke " + vysledokHoduKockou);

        boolean hracPresielStartom = posunHraca(hrac,vysledokHoduKockou);
        if (hracPresielStartom) {
            System.out.println("Presli ste startom a preto ziskavate bonus " + SUMA_OBDRZANA_PRI_PRECHODE_STARTOM);
            hrac.vlozPeniaze(SUMA_OBDRZANA_PRI_PRECHODE_STARTOM);
        }

        int indexPolickaPosunutehoHraca = poziciaHraca.get(hrac.getMeno());
        Policko polickoPosunutehoHraca = hernaPlocha.get(indexPolickaPosunutehoHraca);
        System.out.println("Hrac " + hrac.getMeno() + " je na policku " + typPolicka(polickoPosunutehoHraca));

        try {
            polickoPosunutehoHraca.vykonajAkciu(hrac);
            vazenie.zostavaKol();
            hraci.add(hrac);
        } catch (HracNemaPeniazeException e) {
            System.out.println("Hrac nema peniaze na zaplatenie dani alebo stojneho, vypadava z hry.");
            System.out.println("Uvolnuju sa jeho nehnutelnosti.");
            for (Policko policko : hernaPlocha){
                if (typPolicka(policko).equals("Nehnutelnost")) {
                    ((Nehnutelnost) policko).uvolniAkPatri(hrac);
                }
            }
        }
    }
}