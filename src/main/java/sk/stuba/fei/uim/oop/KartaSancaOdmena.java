package sk.stuba.fei.uim.oop;

public class KartaSancaOdmena implements KartaSanca {
    @Override
    public void vykonajUlohu(Hrac hrac) {
        System.out.println("Nasiel si penazenku a vratil si ju majitelovi. Dostal si odmenu 50 eur.");
        hrac.vlozPeniaze(50);
    }
}
