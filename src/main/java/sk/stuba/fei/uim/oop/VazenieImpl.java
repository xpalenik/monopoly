package sk.stuba.fei.uim.oop;

import java.util.HashMap;
import java.util.Map;

import static sk.stuba.fei.uim.oop.KeyboardInput.readInt;

public class VazenieImpl implements Vazenie {

    private Map<String,Integer> stojiKol = new HashMap<>();

    @Override
    public void vykonajAkciu(Hrac hrac) {
    }

    @Override
    public void uvazni(Hrac hrac) {
        int pocetKol = 0;
        while (pocetKol<=0) {
            pocetKol = readInt("Na kolko kol chcete uvaznit hraca");
            if (pocetKol<=0) System.out.println("Pocet kol musi byt kladne cele cislo.");
        }
        this.stojiKol.put(hrac.getMeno(), pocetKol);
        System.out.println("Hrac "+ hrac.getMeno() + " bol uvazneny na " + pocetKol + " kol/kola.");
    }

    @Override
    public void presloJednoKolo() {
        stojiKol.entrySet().removeIf(entry -> entry.getValue() <= 0);
        stojiKol.replaceAll( (k,v)->v=v-1 );
    }

    @Override
    public void zostavaKol() {
        System.out.println("Pocet uvaznenych hracov: " + stojiKol.size());
        for (Map.Entry<String,Integer> entry: stojiKol.entrySet()) {
            System.out.println("Hracovi "+entry.getKey()+" zostava "+entry.getValue()+" kol/kola vo vazeni.");
        }
    }

    @Override
    public boolean jeVoVazeni(Hrac hrac) {
        return stojiKol.containsKey(hrac.getMeno());
    }


}
