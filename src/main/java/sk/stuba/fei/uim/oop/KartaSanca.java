package sk.stuba.fei.uim.oop;

/**
 * Karta Šanca vykoná akciu na nej napísanú.
 */
public interface KartaSanca {
    public void vykonajUlohu(Hrac hrac);
}
