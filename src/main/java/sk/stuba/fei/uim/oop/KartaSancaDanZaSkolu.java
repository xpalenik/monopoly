package sk.stuba.fei.uim.oop;

public class KartaSancaDanZaSkolu implements KartaSanca {
    @Override
    public void vykonajUlohu(Hrac hrac) throws HracNemaPeniazeException{
        System.out.println("Nová vláda zaviedla daň na školstvo. Musíš zaplatiť 50 eur.");
        hrac.vyberPeniaze(50);
    }
}
