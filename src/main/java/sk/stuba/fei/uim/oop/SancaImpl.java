package sk.stuba.fei.uim.oop;

import java.util.Queue;

public class SancaImpl implements Sanca {

    private Queue<KartaSanca> balicekKarietSanca;

    SancaImpl(Queue<KartaSanca> balicekKarietSancaSingleton){
        this.balicekKarietSanca = balicekKarietSancaSingleton;
    }

    @Override
    public void vykonajAkciu(Hrac hrac) {
        KartaSanca kartaSanca = balicekKarietSanca.poll();
        assert kartaSanca != null;
        balicekKarietSanca.add(kartaSanca);
        kartaSanca.vykonajUlohu(hrac);
    }
}
