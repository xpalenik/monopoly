package sk.stuba.fei.uim.oop;

public interface Hrac {
    public void vlozPeniaze(int suma);
    public void vyberPeniaze(int suma);
    public String getMeno();
    public int vypisZostatok();
}
