package sk.stuba.fei.uim.oop;

public class KartaSancaVyhra implements KartaSanca {
    @Override
    public void vykonajUlohu(Hrac hrac) {
        System.out.println("Vyhral si v sportke 300 eur. A to si ani nepodal listok!");
        hrac.vlozPeniaze(300);
    }
}
