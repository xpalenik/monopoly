package sk.stuba.fei.uim.oop;

public class PoliciaImpl implements Policia {
    private Vazenie vazenie;
    private Hra hra;

    public PoliciaImpl(Vazenie vazenie, Hra hra) {
        this.vazenie = vazenie;
        this.hra = hra;
    }

    @Override
    public void vykonajAkciu(Hrac hrac) {
        hra.presunDoVazenia(hrac);
        vazenie.uvazni(hrac);
    }
}
