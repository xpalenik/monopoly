package sk.stuba.fei.uim.oop;
import static sk.stuba.fei.uim.oop.KeyboardInput.*;

public class NehnutelnostImpl implements Nehnutelnost {
    private int cena;
    private int stojne;
    private Hrac vlastnik = null;
    Hra hra;

    public NehnutelnostImpl(int cena) {
        this.cena = cena;
        this.stojne = (int)(0.2 * cena);
    }

    @Override
    public void vykonajAkciu (Hrac hrac) throws HracNemaPeniazeException{

        if (vlastnik != null) {
            System.out.println("Hrac plati stojne " + stojne + " hracovi " + vlastnik.getMeno());

            try {
                hrac.vyberPeniaze(stojne);
                vlastnik.vlozPeniaze(stojne);
            } catch (HracNemaPeniazeException e) {
                vlastnik.vlozPeniaze(hrac.vypisZostatok());
                throw new HracNemaPeniazeException();
            }

            return;
        }

        System.out.println("Cena nehnutelnosti je " + cena);
        if (cena>hrac.vypisZostatok()){
            System.out.println("Nemate dostatok penazi na kupu nehnutelnosti.");
            return;
        }

        char odpoved = (char) 0;
        while (odpoved != 'a' && odpoved != 'n') {
            odpoved = readChar("Prajete si kupit nehnutelnost? (a/n)");
        }

        if (odpoved == 'a') {
            hrac.vyberPeniaze(cena);
            vlastnik = hrac;
            System.out.println("Kupili ste nehnutelnost.");
        } else {
            System.out.println("Nekupili ste nehnutelnost.");
        }
    }

    @Override
    public void uvolniAkPatri(Hrac hrac) {
        if (vlastnik == hrac){
            vlastnik = null;
        }
    }
}
