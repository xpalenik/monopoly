package sk.stuba.fei.uim.oop;

public interface Vazenie extends Policko {
    public void uvazni(Hrac hrac);
    public void presloJednoKolo();
    public void zostavaKol();
    public boolean jeVoVazeni(Hrac hrac);
}
