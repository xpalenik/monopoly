package sk.stuba.fei.uim.oop;

public class PlatbaDaneImpl implements PlatbaDane {
    private int vyskaDane = 50;

    public PlatbaDaneImpl(int vyskaDane) {
        this.vyskaDane = vyskaDane;
    }

    @Override
    public void vykonajAkciu(Hrac hrac) throws HracNemaPeniazeException{
        System.out.println("Hrac "+hrac.getMeno()+" plati dan "+vyskaDane);
        hrac.vyberPeniaze(vyskaDane);
    }
}
