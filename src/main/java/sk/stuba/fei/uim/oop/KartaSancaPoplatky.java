package sk.stuba.fei.uim.oop;

public class KartaSancaPoplatky implements KartaSanca {
    @Override
    public void vykonajUlohu(Hrac hrac) throws HracNemaPeniazeException{
        System.out.println("Poplatky za vedenie tvojho tucneho konta sa vysplhali na 40 eur. Platíš.");
        hrac.vyberPeniaze(40);
    }
}
